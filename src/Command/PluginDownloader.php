<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use ZipArchive;

class PluginDownloader extends Command
{
    protected static $defaultName = 'codesnippet:install';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
            // the short description shown while running "php bin/console list"
            ->setDescription('Installs the code snippet plugin for CKEditor..')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to download the codesnippet plugin for symfony...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $path = 'public/bundles/fosckeditor/plugins/';
        if (!file_exists($path . '/codesnippet')) {
            $file = 'src/resources/codesnippet/codesnippet_4.13.1.zip';
            $zip = new ZipArchive();
            $zip->open($file);
            $zip->extractTo($path);
            $zip->close();
            $io->success('Downloaded codesnippet');
        }

    }
}
