<?php

namespace App\Repository;

use App\Entity\UserSeenPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserSeenPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSeenPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSeenPost[]    findAll()
 * @method UserSeenPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSeenPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserSeenPost::class);
    }

    // /**
    //  * @return UserSeenPost[] Returns an array of UserSeenPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSeenPost
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
