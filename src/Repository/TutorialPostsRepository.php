<?php

namespace App\Repository;

use App\Entity\TutorialPosts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TutorialPosts|null find($id, $lockMode = null, $lockVersion = null)
 * @method TutorialPosts|null findOneBy(array $criteria, array $orderBy = null)
 * @method TutorialPosts[]    findAll()
 * @method TutorialPosts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TutorialPostsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TutorialPosts::class);
    }

    // /**
    //  * @return TutorialPosts[] Returns an array of TutorialPosts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TutorialPosts
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
