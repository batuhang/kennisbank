<?php

namespace App\Repository;

use App\Entity\UserSeePost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserSeePost|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSeePost|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSeePost[]    findAll()
 * @method UserSeePost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSeePostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserSeePost::class);
    }

    // /**
    //  * @return UserSeePost[] Returns an array of UserSeePost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSeePost
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
