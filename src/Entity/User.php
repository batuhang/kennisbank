<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="author")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSeePost", mappedBy="user")
     */
    private $userSeePosts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSeenPost", mappedBy="user")
     */
    private $userSeenPosts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TutorialPosts", mappedBy="author")
     */
    private $tutorialPosts;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->userSeePosts = new ArrayCollection();
        $this->userSeenPosts = new ArrayCollection();
        $this->tutorialPosts = new ArrayCollection();
        // your own logic
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAuthor($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getAuthor() === $this) {
                $post->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserSeePost[]
     */
    public function getUserSeePosts(): Collection
    {
        return $this->userSeePosts;
    }

    public function addUserSeePost(UserSeePost $userSeePost): self
    {
        if (!$this->userSeePosts->contains($userSeePost)) {
            $this->userSeePosts[] = $userSeePost;
            $userSeePost->setUser($this);
        }

        return $this;
    }

    public function removeUserSeePost(UserSeePost $userSeePost): self
    {
        if ($this->userSeePosts->contains($userSeePost)) {
            $this->userSeePosts->removeElement($userSeePost);
            // set the owning side to null (unless already changed)
            if ($userSeePost->getUser() === $this) {
                $userSeePost->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserSeenPost[]
     */
    public function getUserSeenPosts(): Collection
    {
        return $this->userSeenPosts;
    }

    public function addUserSeenPost(UserSeenPost $userSeenPost): self
    {
        if (!$this->userSeenPosts->contains($userSeenPost)) {
            $this->userSeenPosts[] = $userSeenPost;
            $userSeenPost->setUser($this);
        }

        return $this;
    }

    public function removeUserSeenPost(UserSeenPost $userSeenPost): self
    {
        if ($this->userSeenPosts->contains($userSeenPost)) {
            $this->userSeenPosts->removeElement($userSeenPost);
            // set the owning side to null (unless already changed)
            if ($userSeenPost->getUser() === $this) {
                $userSeenPost->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TutorialPosts[]
     */
    public function getTutorialPosts(): Collection
    {
        return $this->tutorialPosts;
    }

    public function addTutorialPost(TutorialPosts $tutorialPost): self
    {
        if (!$this->tutorialPosts->contains($tutorialPost)) {
            $this->tutorialPosts[] = $tutorialPost;
            $tutorialPost->setAuthor($this);
        }

        return $this;
    }

    public function removeTutorialPost(TutorialPosts $tutorialPost): self
    {
        if ($this->tutorialPosts->contains($tutorialPost)) {
            $this->tutorialPosts->removeElement($tutorialPost);
            // set the owning side to null (unless already changed)
            if ($tutorialPost->getAuthor() === $this) {
                $tutorialPost->setAuthor(null);
            }
        }

        return $this;
    }
}