<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TutorialRepository")
 */
class Tutorial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TutorialPosts", mappedBy="tutorial")
     */
    private $tutorialPosts;

    public function __construct()
    {
        $this->tutorialPosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|TutorialPosts[]
     */
    public function getTutorialPosts(): Collection
    {
        return $this->tutorialPosts;
    }

    public function addTutorialPost(TutorialPosts $tutorialPost): self
    {
        if (!$this->tutorialPosts->contains($tutorialPost)) {
            $this->tutorialPosts[] = $tutorialPost;
            $tutorialPost->setTutorial($this);
        }

        return $this;
    }

    public function removeTutorialPost(TutorialPosts $tutorialPost): self
    {
        if ($this->tutorialPosts->contains($tutorialPost)) {
            $this->tutorialPosts->removeElement($tutorialPost);
            // set the owning side to null (unless already changed)
            if ($tutorialPost->getTutorial() === $this) {
                $tutorialPost->setTutorial(null);
            }
        }

        return $this;
    }
}
