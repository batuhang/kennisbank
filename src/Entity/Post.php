<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true))
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, unique=true))
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visibility;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSeePost", mappedBy="post")
     */
    private $userSeePosts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSeenPost", mappedBy="post")
     */
    private $userSeenPosts;

    public function __construct()
    {
        $this->userSeePosts = new ArrayCollection();
        $this->userSeenPosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function setName(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getVisibility(): ?bool
    {
        return $this->visibility;
    }

    public function setVisibility(bool $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|UserSeePost[]
     */
    public function getUserSeePosts(): Collection
    {
        return $this->userSeePosts;
    }

    public function addUserSeePost(UserSeePost $userSeePost): self
    {
        if (!$this->userSeePosts->contains($userSeePost)) {
            $this->userSeePosts[] = $userSeePost;
            $userSeePost->setPost($this);
        }

        return $this;
    }

    public function removeUserSeePost(UserSeePost $userSeePost): self
    {
        if ($this->userSeePosts->contains($userSeePost)) {
            $this->userSeePosts->removeElement($userSeePost);
            // set the owning side to null (unless already changed)
            if ($userSeePost->getPost() === $this) {
                $userSeePost->setPost(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|UserSeenPost[]
     */
    public function getUserSeenPosts(): Collection
    {
        return $this->userSeenPosts;
    }

    public function addUserSeenPost(UserSeenPost $userSeenPost): self
    {
        if (!$this->userSeenPosts->contains($userSeenPost)) {
            $this->userSeenPosts[] = $userSeenPost;
            $userSeenPost->setPost($this);
        }

        return $this;
    }

    public function removeUserSeenPost(UserSeenPost $userSeenPost): self
    {
        if ($this->userSeenPosts->contains($userSeenPost)) {
            $this->userSeenPosts->removeElement($userSeenPost);
            // set the owning side to null (unless already changed)
            if ($userSeenPost->getPost() === $this) {
                $userSeenPost->setPost(null);
            }
        }

        return $this;
    }
}
