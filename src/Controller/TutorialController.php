<?php

namespace App\Controller;

use App\Entity\Tutorial;
use App\Entity\TutorialPosts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TutorialController extends AbstractController
{
    /**
     * @Route("/tutorials", name="tutorials")
     */
    public function index()
    {
        $tutorials = $this->getDoctrine()->getManager()->getRepository(Tutorial::class)->findAll();

        return $this->render('tutorial/index.html.twig', [
            'tutorials' => $tutorials,
        ]);
    }

    /**
     * @Route("/tutorial/{slug}", name="tutorial_show")
     */
    public function show(string $slug)
    {
        $tutorial = $this->getDoctrine()->getManager()->getRepository(Tutorial::class)->findOneBy(['slug' => $slug]);

        if(!$tutorial){
            return $this->redirectToRoute('error_post_not_found');
        }

        $posts = $this->getDoctrine()->getManager()->getRepository(TutorialPosts::class)->findBy(['tutorial' => $tutorial], ['listorder' => 'ASC'] );
        return $this->render('tutorial/show.html.twig', [
            'tutorial' => $tutorial,
            'posts' => $posts
        ]);
    }
}
