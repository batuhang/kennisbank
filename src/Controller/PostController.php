<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\UserSeenPost;
use App\Entity\UserSeePost;
use App\Form\CreatePostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/kennisbank/{category}/{post}", name="post_show")
     */
    public function show(string $category, string $post)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy(['slug' => $category]);

        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findOneBy(['slug' => $post, 'category' => $category]);

        $post_user_see = $this->getDoctrine()
            ->getRepository(UserSeePost::class)
            ->findOneBy(['post' => $post, 'user' => $this->getUser(), 'visible' => true]);

        if ($this->getUser()) {
            $post_user_seen = $this->getDoctrine()
                ->getRepository(UserSeenPost::class)
                ->findOneBy(['post' => $post, 'user' => $this->getUser()]);

            if (!$post_user_seen) {
                $seen = new UserSeenPost();
                $seen->setPost($post);
                $seen->setUser($this->getUser());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($seen);
                $entityManager->flush();
            }
        }

        if (!$post) {
            return $this->redirectToRoute('error_post_not_found');
        }

        if (!$post->getVisibility()) {
            if ($post_user_see) {
                return $this->render('post/show.html.twig', [
                    'post' => $post,
                    'category' => $category
                ]);
            }
            if ($this->getUser() == $post->getAuthor()) {
                return $this->render('post/show.html.twig', [
                    'post' => $post,
                    'category' => $category
                ]);
            }
            return $this->redirectToRoute('error_private_post');
        }

        return $this->render('post/show.html.twig', [
            'post' => $post,
            'category' => $category
        ]);
    }

    /**
     * @Route("/profile/post/create", name="post_create")
     */
    public function createPost(Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('error_log_in');
        }

        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $form = $this->createForm(CreatePostType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $category = $this->getDoctrine()
                ->getRepository(Category::class)
                ->findOneBy(['name' => $formData['category']]);

            $post = $this->getDoctrine()
                ->getRepository(Post::class);

            if ($post->findOneBy(['title' => $formData['title']]) !== null) {

                $form->addError(new FormError("Title already taken"));

            } else {
                if (!$category) {
                    $form->addError(new FormError("Category doesn't exist"));
                } else {
                    $post = new Post();
                    $post->setAuthor($this->getUser());
                    $post->setCategory($category);
                    $post->setContent($formData['content']);
                    $post->setTitle($formData['title']);
                    $post->setSlug(strtolower(str_replace(' ', '-', $formData['title'])));
                    $post->setVisibility($formData['visibility'] ? true : false);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($post);
                    $entityManager->flush();

                    return $this->redirectToRoute('post_show', [
                        'category' => $post->getCategory()->getSlug(),
                        'post' => $post->getSlug()
                    ]);
                }
            }
        }
        return $this->render('user/create_post.html.twig', [
            'categories' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile/post/edit/{id}", name="post_edit")
     */
    public function edit(string $id, Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('error_log_in');
        }
        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findOneBy(['id' => $id]);

        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        if (!$post) {
            return $this->redirectToRoute('error_post_not_found');
        }

        if ($this->getUser() !== $post->getAuthor()) {
            return $this->redirectToRoute('error_post_not_found');
        }

        $form = $this->createForm(CreatePostType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $category = $this->getDoctrine()
                ->getRepository(Category::class)
                ->findOneBy(['name' => $formData['category']]);

            if (!$category) {
                $form->addError(new FormError("Category doesn't exist"));
            } else {

                $post->setAuthor($this->getUser());
                $post->setCategory($category);
                $post->setContent($formData['content']);
                $post->setTitle($formData['title']);
                $post->setSlug(strtolower(str_replace(' ', '-', $formData['title'])));
                $post->setVisibility($formData['visibility'] ? true : false);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($post);
                $entityManager->flush();

                return $this->redirectToRoute('post_show', [
                    'category' => $post->getCategory()->getSlug(),
                    'post' => $post->getSlug()
                ]);
            }
        }
        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'categories' => $category
        ]);
    }
}
