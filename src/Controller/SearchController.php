<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search/user/{username}/", name="search_user")
     */
    public function user_index(string $username, UserRepository $userRepository)
    {
        $user = $userRepository->searchUsers($username);

        return $this->render('search/search_user.html.twig', [
            'users' => $user,
        ]);
    }

    /**
     * @Route("/search/user/{username}/profile", name="search_user_profile")
     */
    public function user_profile_show(string $username)
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['username' => $username]);

        if(!$user){
            return $this->redirectToRoute('default');
        }

        return $this->render('search/search_user_profile.html.twig', [
            'user' => $user,
        ]);
    }
}
