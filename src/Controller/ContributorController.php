<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ContributorController extends AbstractController
{
    /**
     * @Route("/contributor", name="contributor")
     */
    public function index()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('error_log_in');
        }

        if(!$this->getUser()->hasRole('ROLE_CONTRIBUTOR')){
            return $this->redirectToRoute('error_log_in');
        }

        return $this->render('contributor/index.html.twig', [
            'controller_name' => 'ContributorController',
        ]);
    }
}
