<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\UserSeenPost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category/{slug}", name="category")
     */
    public function show_category_posts(string $slug)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy(['slug' => $slug]);

        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['category' => $category, 'visibility' => true]);

        $user_seen_post = false;
        return $this->render('category/index.html.twig', [
            'posts' => $post,
            'user_seen_post' => $user_seen_post
        ]);
    }
}
