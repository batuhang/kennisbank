<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Form\CreatePostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function profile()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('error_log_in');
        }

        return $this->render('user/profile.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/profile/posts", name="profile_posts")
     */
    public function myPosts()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('error_log_in');
        }

        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['author' => $this->getUser()->getId()]);

        return $this->render('user/ego.html.twig', [
            'user' => $this->getUser(),
            'posts' => $posts
        ]);
    }
}
