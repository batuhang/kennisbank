<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{
    /**
     * @Route("/error/page-not-found/", name="error_not_found")
     */
    public function notFound()
    {
        return $this->render('error/index.html.twig', [
        ]);
    }

    /**
     * @Route("/error/post-not-found/", name="error_post_not_found")
     */
    public function incorrectUrl()
    {
        return $this->render('error/index.html.twig', [
        ]);
    }

    /**
     * @Route("/error/post-is-private/", name="error_private_post")
     */
    public function privatePost()
    {
        return $this->render('error/private.html.twig', [
        ]);
    }

    /**
     * @Route("/error/please-log-in/", name="error_log_in")
     */
    public function notLoggedIn(){
        return $this->render('error/login.html.twig', [
        ]);
    }
}
